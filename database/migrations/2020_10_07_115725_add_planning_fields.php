<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPlanningFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table("planning", function (Blueprint $table){
            if (!Schema::hasColumn('planning', 'emission_id')) {
                $table->unsignedBigInteger('emission_id')->nullable()->after('link');
                $table->foreign("emission_id")->references('id')->on('emissions')->onDelete('cascade');
            }
            if (!Schema::hasColumn('planning', 'link')) {
                $table->dropColumn('link');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
}
