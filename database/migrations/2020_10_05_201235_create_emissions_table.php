<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emissions', function (Blueprint $table) {
            $table->id();
            $table->string('name',191);
            $table->string('description',191);
            $table->string('image',191)->nullable();
            $table->string('facebook',191)->nullable();
            $table->string('twitter',191)->nullable();
            $table->string('instagram',191)->nullable();
            $table->string('youtube',191)->nullable();
            $table->string('website',191)->nullable();
            $table->string('twitch',191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emissions');
    }
}
