const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/apysey/app.js", "public/assets/js/apysey.js")
    .webpackConfig({
        resolve: {
            alias: {
                '@apysey': path.resolve(__dirname, 'vendor/dysey/apysey/resources/assets/js/'),
            },
        },
    })
    .version()
    .options({
        processCssUrls: false
    });
