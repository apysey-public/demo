@extends('apysey::errors.minimal')

@section('title', __('Service Unavailable'))
@section('code', '503')
@section('message', __($message ?: 'Service Unavailable'))
