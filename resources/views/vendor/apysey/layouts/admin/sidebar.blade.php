<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="brand-link">
        <a href="/" class="pointer">
            @if(config("apysey.custom_logo"))
                <img src="{{ asset(config("apysey.custom_logo")) }}" alt="Logo" class="brand-image" />
            @else
                <img src="{{ asset("vendor/apysey/images/apysey-icon.png") }}" alt="Apysey Logo" class="brand-image" />
            @endif
            <span class="brand-text font-weight-light">{{ config("apysey.custom_title") }}</span>
        </a>

        <a class="brand-link-hamburger" v-on:click="triggerMenu" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
    </div>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <router-link to="/" class="nav-link" active-class="" exact-active-class="active">
                        <i class="nav-icon fas fa-home"></i>
                        <p>@lang("apysey::app.apysey.sidebar.dashboard")</p>
                    </router-link>
                </li>
                {{--@permission("show_langs")--}}
                    {{--<li class="nav-item">--}}
                        {{--<router-link tag="a" to="/langs" class="nav-link">--}}
                            {{--<i class="nav-icon fas fa-flag"></i>--}}
                            {{--<p>@lang("apysey::app.apysey.sidebar.languages")</p>--}}
                        {{--</router-link>--}}
                    {{--</li>--}}
                {{--@endpermission--}}
                @foreach (\Dysey\Apysey\Managers\ResourcesManager::$resources as $element => $data)
                    @if($data::$visible)
                    @permission($data::$permission ?? "show_" . $data::$name)
                        <li class="nav-item">
                            <router-link to="{{ $data::$route ?? '/resource/' .\Illuminate\Support\Str::lower($data::$name)}}" class="nav-link">
                                <i class="nav-icon {{ $data::$icon ?? "fas fa-database" }}"></i>
                                <p>{{ $data::$label ?? \Illuminate\Support\Str::lower($data::$name) }}</p>
                            </router-link>
                        </li>
                    @endpermission
                    @endif
                @endforeach
                @foreach (config("apysey.packages") as $element => $data)
                    @if(isset($data['navigation']) && !empty($data['navigation']))
                        @foreach($data['navigation'] as $index => $nav)
                            @permission($nav["permission"] ?? "show_" . $index)
                                <li class="nav-item">
                                    <router-link to="/{{ \Illuminate\Support\Str::lower($nav['route']) }}" class="nav-link">
                                        <i class="nav-icon {{ $nav["icon"] ?? "fas fa-database" }}"></i>
                                        <p>{{ $nav["label"] ?? \Illuminate\Support\Str::lower($index) }}</p>
                                    </router-link>
                                </li>
                            @endpermission
                        @endforeach
                    @endif
                @endforeach
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->

    <div class="sidebar-bottom">
        <a class="d-block" href="javascript:{}" onclick="document.getElementById('logout-form').submit();">@lang("apysey::app.apysey.sidebar.logout")</a>
        <form id="logout-form" action="{{ route('apysey.logout') }}" method="POST" style="display: none;">
            @csrf
        </form>

        <!-- #TODO -->
        <select class="form-control" v-on:change="onChangeLang">
            <option v-for="lang in apyseyLangs" :value="lang" :selected="currentLang === lang">
                @{{ lang }}
            </option>
        </select>

        <strong>Copyright &copy; {{ now()->year }} Apysey</strong>. @lang("apysey::app.apysey.sidebar.all_rights_reserved")
    </div>
</aside>
