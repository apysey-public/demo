@extends('apysey::core.app')

@section('content')
    @include('apysey::layouts.admin.sidebar')
    @include('apysey::layouts.admin.content')
@endsection
