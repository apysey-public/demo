<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planning extends Model
{
    protected $table = "planning";
    public $timestamps = true;

    public function emission() {
        return $this->belongsTo(Emission::class, "emission_id");
    }
}
