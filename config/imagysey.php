<?php

return [
    'route' => env('IMAGYSEY_PATH', 'imagyseycache'),
    'key' => env('IMAGYSEY_KEY', null),

    'disks' => array(
        'public',
    ),

    'dynamicSize' => false,

    'middleware' => [
        'web',
        'auth',
    ],

    'namespace' => [
        'controller' => '/Dysey/Apysey/Http/Controllers/Imagysey',
    ],

    'templates' => array(
        'small' => 'Dysey/Imagysey/Filters/ImageFilters/Small',
        'medium' => 'Dysey/Imagysey/Filters/ImageFilters/Medium',
        'large' => 'Dysey/Imagysey/Filters/ImageFilters/Large',
        'original' => 'Dysey/Imagysey/Filters/ImageFilters/Original',
        'greyscale' => 'Dysey/Imagysey/Filters/ImageFilters/GreyScale',
        'adminpanel' => 'Dysey/Imagysey/Filters/ImageFilters/AdminPanel',
    ),

    'lifetime' => 43200,
];