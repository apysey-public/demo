<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersDemo extends Model
{
    use HasFactory;

    protected $table = "users_demo";

    public function emissions() {
        return $this->belongsToMany(Emission::class, "emission_user");
    }
}
