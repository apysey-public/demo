<?php
/**
 * Twitter: @Dysey_Qanga
 * Date: 02/03/2020
 * Time: 15:12
 */

namespace App\Providers;

use Dysey\Apysey;
use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;

class ApyseyServiceProvider  extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->booted(function () {
            $schedule = resolve(Schedule::class);
            $schedule->command('apysey:digest')
                ->weekly()
                ->mondays()
                ->timezone(config('app.timezone'))
                ->at('08:00')
                ->when(function () {
                    return config('apysey.digest.enabled');
                });
        });
    }
}
