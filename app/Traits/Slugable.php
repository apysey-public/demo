<?php
/**
 * Twitter: @Dysey_Qanga
 * Date: 11/11/2020
 * Time: 16:01
 */

namespace App\Traits;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

trait Slugable
{
    public function addSlug($modelInstance, $entity, Model $model, $column) {
        $slug = url_custom_encode($entity->$column);
        $exist = $model->where('slug', $slug)->where("id", "!=", $modelInstance->id)->count();
        while ($exist > 0) {
            $slug = url_custom_encode($entity->$column) . "_" . generateCode(8);
            $exist = $model->where('slug', $slug)->count();
        }
        $modelInstance->slug = $slug;

        return $modelInstance;
    }
}