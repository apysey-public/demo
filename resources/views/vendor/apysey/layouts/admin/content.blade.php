<div class="content-wrapper">
    @if(!$assetsUpToDate)
        <div class="alert alert-danger border-0 text-center rounded-0">
            {{ __('apysey::app.apysey.assets_are_not_up_to_date') }}
            {{ __('apysey::app.apysey.to_update_run') }}<br/>php artisan apysey:publish
        </div>
    @endif

    <router-view></router-view>
</div>
