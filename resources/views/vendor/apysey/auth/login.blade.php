@extends('apysey::core.app')

@section('content')
<div class="login-page">
    <div class="login-box">
        <div class="login-logo">
            @if(config("apysey.custom_logo"))
                <img src="{{ asset(config("apysey.custom_logo")) }}" height="64" alt="Logo" />
            @else
                <img src="{{ asset("vendor/apysey/images/apysey-icon.png") }}" height="64" alt="Apysey" />
            @endif
        </div>

        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">@lang("apysey::app.apysey.login page.title")</p>

                <form class="mt-4" method="POST" action="{{ route('apysey::auth.login') }}">
                    @csrf

                    <div class="input-group mb-3">
                        <input type="email" class="form-control{{$errors->has('email') ? ' is-invalid' : ''}}" placeholder="@lang("apysey::app.apysey.login page.email")"
                               id="email" name="email" value="admin@admin.com" required readonly>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    <div class="input-group mb-3">
                        <input type="password" class="form-control{{$errors->has('password') ? ' is-invalid' : ''}}" placeholder="@lang("apysey::app.apysey.login page.password")"
                               id="password" name="password" value="admin" required readonly>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember">
                                    @lang("apysey::app.apysey.login page.remember")
                                </label>
                            </div>
                        </div>

                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">@lang("apysey::app.apysey.login page.login")</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
