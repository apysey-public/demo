<?php

namespace App\Http\Controllers\Back;

use Dysey\Apysey\Http\Controllers\ResourcesController;
use Dysey\Apysey\Http\Requests\ResourcesRequest;
use Dysey\Apysey\Services\ResourcesService;
use Dysey\Apysey\Traits\Indexable;
use Dysey\Apysey\Traits\ManageDataTrait;

class UserController extends ResourcesController
{
    use Indexable, ManageDataTrait;
    /**
     * @var ResourcesService
     */
    private $resourcesService;

    public function __construct(ResourcesService $resourcesService) {
        $this->resourcesService = $resourcesService;
        parent::__construct($resourcesService);
    }

    public function index(ResourcesRequest $request, $resourceName, $fromDefinedController = true) {
        return parent::index($request, $resourceName, $fromDefinedController);
    }

    public function show(ResourcesRequest $request, $resourceName, $resourceId, $fromDefinedController = true) {
        return parent::show($request, $resourceName, $resourceId, $fromDefinedController);
    }

    public function create(ResourcesRequest $request, $resourceName, $fromDefinedController = true) {
        return parent::create($request, $resourceName, $fromDefinedController);
    }

    public function save(ResourcesRequest $request, $resourceName, $fromDefinedController = true) {
        $resource = $request->newResource();

        if (isset($resource::$controller) && !is_null($resource::$controller) && !$fromDefinedController) {
            return $this->callOverrideMethod($request, $resource::$controller);
        }

        $parameters = $this->getParameters($request, $resource->infos());
        $parameters['columns'] = $resource->columns($request);

        // get columns
        $columns = collect($parameters["columns"])->filter(function ($value, $key) {
            if ((isset($value->showOnCreation) && $value->showOnCreation) || !isset($value->showOnCreation))
                return $value;
        });

        $modelInstance = new $parameters["model"]();
        $entity = json_decode($request->entity);

        $modelInstance = $this->saveDatas($columns, $modelInstance,$entity);

        $modelInstance->password = bcrypt($request->password);

        $modelInstance->save();
        $modelInstance = $this->saveRelations($columns,$modelInstance, $entity);

        $modelInstance = $this->saveFile($request, $modelInstance, $resource);

        return $modelInstance;
    }

    public function edit(ResourcesRequest $request, $resourceName, $resourceId, $fromDefinedController = true) {
        return parent::edit($request, $resourceName, $resourceId, $fromDefinedController);
    }

    public function update(ResourcesRequest $request, $resourceName, $resourceId, $fromDefinedController = true) {
        return parent::update($request, $resourceName, $resourceId, $fromDefinedController);
    }

    public function delete(ResourcesRequest $request, $resourceName, $resourceId, $fromDefinedController = true) {
        return parent::delete($request, $resourceName, $resourceId, $fromDefinedController);
    }
}
