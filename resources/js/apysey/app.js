import UserUpdate from "./components/users/update"

Apysey.booting((Vue, router) => {
    router.addRoutes([
        {
            name: "userUpdate",
            path: "/users/update/:id",
            component: UserUpdate
        }
    ]);
});