<?php

namespace App\Apysey;

use App\Http\Controllers\Back\UserController;
use App\Models\User;
use App\Models\UsersDemo;
use Dysey\Apysey\Abstracts\AbstractResources;
use Dysey\Apysey\Fields\Datetime;
use Dysey\Apysey\Fields\Password;
use Dysey\Apysey\Fields\Text;
use Dysey\Apysey\Filters\PanelFilters\DateTimeFilter;
use Dysey\Apysey\Filters\PanelFilters\TextFilter;
use Illuminate\Http\Request;

class Users extends AbstractResources
{

    public static $model = UsersDemo::class;

    public static $name = "users";

    public static $label = "Users";
    public static $icon = "fas fa-user";
    public static $orderBy = "created_at";
    public static $direction = "desc";

    public static $controller = UserController::class;

    public static $redirectEdit = "/users/update";

    public function rules() : array {
        return [

        ];
    }

    public function rulesCreation() : array {
        return [

        ];
    }

    public function rulesUpdate() : array {
        return [

        ];
    }

    public function columns(Request $request) : array {
        return [
            Text::make("Name", "name")->filter(new TextFilter()),
            Text::make("Email", "email")->filter(new TextFilter()),
            Password::make("Password", "password")->onlyOnForms()->hideWhenUpdating(),
            Datetime::make("Created at", "created_at")->filter(new DateTimeFilter())->exceptOnForms(),
        ];
    }
}
