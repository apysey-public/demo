<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmissionTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emission_user', function (Blueprint $table) {
            $table->unsignedBigInteger("emission_id");
            $table->unsignedBigInteger("user_id");
            $table->foreign("emission_id")->references('id')->on('emissions')->onDelete('cascade');
            $table->foreign("user_id")->references('id')->on('users_demo')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emission_team');
    }
}
