@extends('apysey::errors.minimal')

@section('title', __('Forbidden'))
@section('code', '403')
@section('message', __($message ?: 'Forbidden'))
