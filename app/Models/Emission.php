<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Emission extends Model
{
    use HasFactory;

    public function plannings() {
        return $this->hasMany(Planning::class,'emission_id');
    }

    public function users() {
        return $this->belongsToMany(UsersDemo::class, "emission_user", "emission_id", "user_id");
    }
}
