<?php

return [

    'locales' => ['en','fr',],

    'default_login' => true,

    'custom_logo' => null,
    'custom_title' => 'Apysey',

    'path' => env('APYSEY_PATH', 'admin'),

    'resourcesPath' => app_path("Apysey"),
    'resourcesNamespace' => "App\\Apysey",

    'middleware' => [
        'web',
        'auth.custom',
        'locale'
    ],

    'default_disk' => env('DEFAULT_STORAGE_DISK', 'public'),
    'default_visibility' => env('DEFAULT_VISIBILITY_DISK', 'public'),

    'pagination' => 10,

    'namespace' => [
        'controller' => '\\Dysey\\Apysey\\Http\\Controllers',
        'resources' => 'App\\Apysey'
    ],

    //Waiting an Array
    /*
     *
     *
        [
            "js" => "",
            "css" => "",
            "navigation" => [
                "" => [
                    "label" => "",
                    "icon" => ",
                    "route" => "",
                    "permission" => ""
                ]
            ],
        ]
     *
     */
    'packages' => [
        [
            "js" => "assets/js/apysey.js",
        ]
    ]

];

?>
