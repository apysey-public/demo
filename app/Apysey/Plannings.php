<?php
/**
 * Twitter: @Dysey_Qanga
 * Date: 14/11/2020
 * Time: 17:49
 */

namespace App\Apysey;


use App\Http\Controllers\Back\Apysey\EmissionController;
use App\Models\Emission;
use App\Models\Planning;
use Dysey\Apysey\Abstracts\AbstractResources;
use Dysey\Apysey\Fields\BelongsTo;
use Dysey\Apysey\Fields\BelongsToMany;
use Dysey\Apysey\Fields\Datetime;
use Dysey\Apysey\Fields\Image;
use Dysey\Apysey\Fields\Link;
use Dysey\Apysey\Fields\Text;
use Dysey\Apysey\Fields\Textarea;
use Dysey\Apysey\Fields\Time;
use Dysey\Apysey\Filters\PanelFilters\DateTimeFilter;
use Dysey\Apysey\Filters\PanelFilters\SelectMultiple;
use Dysey\Apysey\Filters\PanelFilters\TextFilter;
use Illuminate\Http\Request;

class Plannings extends AbstractResources
{
    public static $model = Planning::class;

    public static $name = "plannings";

    public static $label = 'Plannings';
    public static $icon = "fas fa-calendar-alt";
    public static $orderBy = "day";
    public static $direction = "asc";


    public function rules() : array {
        return [

        ];
    }

    public function rulesCreation() : array {
        return [

        ];
    }

    public function rulesUpdate() : array {
        return [

        ];
    }

    public function columns(Request $request) : array {
        return [
            Text::make("Title", "title")->filter(new TextFilter()),
            Text::make("Day", "day")->filter(new SelectMultiple(["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"])),
            Image::make("Image", "image"),
            Time::make("Begin", "begin"),
            Time::make("End", "end"),
            BelongsTo::make("Emission", "emission", "emission_id", "emissions", "name"),
        ];
    }
}