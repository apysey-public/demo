<?php

namespace App\Apysey;

use App\Http\Controllers\Back\EmissionController;
use App\Models\Emission;
use Dysey\Apysey\Abstracts\AbstractResources;
use Dysey\Apysey\Fields\BelongsToMany;
use Dysey\Apysey\Fields\Datetime;
use Dysey\Apysey\Fields\Image;
use Dysey\Apysey\Fields\Link;
use Dysey\Apysey\Fields\Text;
use Dysey\Apysey\Fields\Textarea;
use Dysey\Apysey\Filters\PanelFilters\DateTimeFilter;
use Dysey\Apysey\Filters\PanelFilters\TextFilter;
use Illuminate\Http\Request;

class Emissions extends AbstractResources
{

    public static $model = Emission::class;

    public static $name = "emissions";

    public static $label = "Emissions";
    public static $icon = "fas fa-stream";
    public static $orderBy = "created_at";
    public static $direction = "desc";
    public static $controller = EmissionController::class;


    public function rules() : array {
        return [

        ];
    }

    public function rulesCreation() : array {
        return [

        ];
    }

    public function rulesUpdate() : array {
        return [

        ];
    }

    public function columns(Request $request) : array {
        return [
            Text::make("Name", "name")->filter(new TextFilter()),
            Textarea::make("Description", "description")->hideFromIndex(),
            Image::make("Image", "image"),
            Link::make("Facebook", "facebook"),
            Link::make("Twitter", "twitter"),
            Link::make("Instagram", "instagram"),
            Link::make("Youtube", "youtube"),
            Link::make("Twitch", "twitch"),
            Link::make("Site", "website"),
            BelongsToMany::make("Chroniclers", "users", "users", "name"),
            Datetime::make("Created at", "created_at")->filter(new DateTimeFilter()),
        ];
    }
}
