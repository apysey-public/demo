<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Apysey') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ mix('css/app.css', 'vendor/apysey') }}" rel="stylesheet">

        @foreach(config("apysey.packages") as $pack)
            @if(isset($pack['css']) && !empty($pack['css']))
                <link href="{{  asset($pack['css']) }}" rel="stylesheet">
            @endif
        @endforeach
    </head>
    <body class="sidebar-mini layout-fixed">
        <!-- wrapper -->
        <div class="wrapper" id="app">
            @yield('content')
        </div>
        <!-- ./wrapper -->

        <!-- Scripts -->
        <script>
            window.Apy = @json($scripts ?? '');
        </script>
        <script src="{{ mix('js/app.js', 'vendor/apysey') }}"></script>


        <script>
            window.Apysey = new CreateApysey();
        </script>


        @foreach(config("apysey.packages") as $pack)
            <script src="{{ asset($pack['js']) }}"></script>
        @endforeach

        <script>
            Apysey.initApyseyVue()
        </script>
        <!-- ./Scripts -->
    </body>
</html>
